
public class Test {

	public static void main(String[] args) {
		
		Raumschiff klingonen = new Raumschiff("IKS Hegh´ta", 1, 100, 100, 100, 100, 2);
		
		Ladung ladungk1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ladungk2 = new Ladung("Bat`leth Klingonen Schwert", 200);
		
		klingonen.addLadung(ladungk1);
		klingonen.addLadung(ladungk2);
		
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
		
		Ladung ladungr1 = new Ladung("Borg Schrott", 5);
		Ladung ladungr3 = new Ladung("Rote Materie", 2);
		Ladung ladungr2 = new Ladung("Plasma Waffe", 50);
		
		klingonen.addLadung(ladungr1);
		klingonen.addLadung(ladungr2);
		klingonen.addLadung(ladungr3);
		
		
		Raumschiff vulkanier = new Raumschiff("N/Var", 0, 80, 80, 50, 100, 5);
		
		Ladung ladungv1 = new Ladung("Forschungssonde", 35);
		Ladung ladungv2 = new Ladung("Photonentorpedo", 3);
		
		klingonen.addLadung(ladungv1);
		klingonen.addLadung(ladungv2);

	

	//Alle einzelnen Aktivitäten werden hier mit Methoden aufgerufen.
	klingonen.photonentorpedosAbschießen(romulaner);
	romulaner.phaserkanonenAbschießen(klingonen);
	vulkanier.nachrichtanalle("Gewalt ist nicht logisch!");
	klingonen.zustandRaumschiff();
	klingonen.ladungsverzeichnisAusgeben();
	klingonen.photonentorpedosAbschießen(romulaner);
	klingonen.photonentorpedosAbschießen(romulaner);
	klingonen.zustandRaumschiff();
	romulaner.zustandRaumschiff();
	vulkanier.zustandRaumschiff();
	klingonen.ladungsverzeichnisAusgeben();
	romulaner.ladungsverzeichnisAusgeben();
	vulkanier.ladungsverzeichnisAusgeben();
	System.out.println(Raumschiff.Logbucheintraegerueckgabe());
	
	}
}
