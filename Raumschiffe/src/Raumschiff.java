import java.util.ArrayList;

public class Raumschiff {

	// public static void main(String[] args) {

	// Variablen, welche vorher bestimmt werden. Inklusive BroadcastKommunikator.

	private String schiffsName;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	private int photonentorpedosAbschießen;

	// Konstruktor Erstellung.

	public Raumschiff(String schiffsName, int photonentorpedoAnzahl, int energieversorgungInProzent,
			int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl) {
		this.schiffsName = schiffsName;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
	}

	// Getter und Setter erstellen.

	public void setSchiffsName(String schiffsName) { // Set
		this.schiffsName = schiffsName;
	}

	public String getSchiffsName() { // Get
		return this.schiffsName;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl2) { // Set
		photonentorpedoAnzahl = photonentorpedoAnzahl2;
	}

	public int getPhotonentorpedoAnzahl() { // Get
		return photonentorpedoAnzahl;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent2) { // Set
		energieversorgungInProzent = energieversorgungInProzent2;
	}

	public int getEnergieversorgungInProzent() { // Get
		return energieversorgungInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent2) { // Set
		schildeInProzent = schildeInProzent2;
	}

	public int getSchildeInProzent() { // Get
		return schildeInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent2) { // Set
		huelleInProzent = huelleInProzent2;
	}

	public int getHuelleInProzent() { // Get
		return huelleInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent2) { // Set
		lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent2;
	}

	public int getLebenserhaltungssystemeInProzent() { // Get
		return lebenserhaltungssystemeInProzent;
	}

	public void setAndroidenAnzahl(int androidenAnzahl2) { // Set
		androidenAnzahl = androidenAnzahl2;
	}

	public int getAndroidenAnzahl() { // Get
		return androidenAnzahl;
	}

	public void addLadung(Ladung neueLadung) { // Ladung wird zum Raumschiff hinzugefügt.
		ladungsverzeichnis.add(neueLadung);
	}
	
	//Aufgabe: Der Zustand des Raumschiffes wird ausgegeben.
	public void zustandRaumschiff() { // Der Zustand des Raumschiffes wird hier ausgegeben.
		System.out.println("Zustand des Raumschiffes: " + this.getSchiffsName() + " :"
				+ "\nAnzahl der Pothonentorpedos: " + this.getPhotonentorpedoAnzahl()
				+ "\nZustand der Energieversorgung: " + this.getEnergieversorgungInProzent() + "\nZustand der Schilde: "
				+ this.getSchildeInProzent() + "\nZustand des Raumschiffes: " + this.getHuelleInProzent()
				+ "\nZustand der Lebenserhaltungssysteme: " + this.getLebenserhaltungssystemeInProzent()
				+ "\nAnzahl der Androiden: " + this.getAndroidenAnzahl());
		// Zudem werden einzelne Zustände und Bestände ausgegeben.

	}

		//Aufgabe: Ladungsverzeichnis ausgeben.
	public void ladungsverzeichnisAusgeben() { // Hier werden die Ladungen angegeben.
		System.out.println("Das Raumschiff " + this.getSchiffsName() + " enthaelt folgende Ladungen: ");
		for (Ladung tmp : ladungsverzeichnis) {
			System.out.println(tmp.getBezeichnung() + " " + tmp.getMenge());
		}

	}
	
	//Aufgabe: Treffer vermerken.
	private void Treffer(Raumschiff r) { //Die Methode Treffer wird hier deklariert und später genutz.
		System.out.println(this.getSchiffsName() + "wurde getroffen!"); 
	}
	
	//Aufgabe: Photonentorpedos werden abgeschossen.
	public void photonentorpedosAbschießen(Raumschiff r) { //Photonentorpedos werden hier abgeschossen.
		if (getPhotonentorpedoAnzahl() <= 0)//Wenn die Anzahl der Photonentorpedos kleiner gleich 0 ist...
			System.out.println("-=*Click*=- \n");//... gebe -=*Click*=- aus.

		else //Ansonsten reduziere  die Anzahl um 1...
			setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() - 1);
		System.out.println("Photonentorpedo wurde abgeschossen! \n"); //... und gebe das aus.
		treffer(r); //Die Methode Treffer wird hier aufgerufen.

	}

	//Aufgabe: Phaserkanonen werden abgeschossen.
	public void phaserkanonenAbschießen(Raumschiff r) { //Phaserkanonen werden hier abgeschossen.
		if (getEnergieversorgungInProzent() < 50) //Solange die Energieversorung unter 50% beträgt, kann die Kanone nicht abgeschossen werden.
			System.out.println("-=*Click*=- \n"); //Wenn die Energie unter 50 % ist gebe -=*Click*=- aus.
		
		else
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50); //Wenn sie mindestens 50% beträgt, kann sie abgeschossen werden.
		System.out.println("Phaserkanone wurde abgeschossen! \n"); //Nach dem Abschuss soll dies ausgegeben werden.
		treffer(r); //Auch hier wird die Methode treffer ausgegeben.

	}

	//Aufgabe: Wenn ein Raumschiff getroffen wird, dann wird eine Nachricht mit dem Schiffsnamen und eine Nachricht mit "wurde getroffen!" ausgegeben.
	private void hit(Raumschiff h) {
		System.out.println(this.getSchiffsName() + "wurde getroffen!");
	}
	//Aufgabe: Eine Nachricht wird zum BroadcastKommunikator hinzugefügt.
	public void nachrichtanalle(String message) {
		broadcastKommunikator.add(message);
	}
	
	//Aufgabe: Logbucheinträge werden zurückgegeben.
	public static ArrayList<String> Logbucheintraegerueckgabe() {
		return broadcastKommunikator;
	}

	//Aufgabe: Nach einem Treffer werden die Schilde des Raumschiffes um 50% geschwächt.
	//		   Wenn die Schilde zerstört sind, so werden 50% von dem Zustand der Hülle und von der Energieversorgung abgezogen.
	
	private void treffer(Raumschiff h) {
		h.setSchildeInProzent(h.getSchildeInProzent()-50);
		if (h.getSchildeInProzent() <= 0) {
			h.setHuelleInProzent(h.getHuelleInProzent()-50);
			h.setEnergieversorgungInProzent(h.getEnergieversorgungInProzent()-50);
			if (h.getHuelleInProzent() <=0) {
				h.setLebenserhaltungssystemeInProzent(0);
				nachrichtanalle("Die Lebenserhaltungssysteme wurden nun vernichtet!");
					
				}
			}
			
		}
	}