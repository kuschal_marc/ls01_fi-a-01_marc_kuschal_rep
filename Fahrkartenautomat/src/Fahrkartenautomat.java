﻿import java.text.DecimalFormat;

import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
      
      
       double zuZahlenderBetrag;			//Hier werden die Variablen bestimmt. 
       double rückgabebetrag;				//Sie werden alle als double angegeben.
        
       zuZahlenderBetrag = fahrkartenbestellungErfassen();	//Ausgelagerte Methode um die Bestellung zu erfassen.
             
       // Geldeinwurf
       // -----------
       
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
 
       
       // Fahrscheinausgabe
       // -----------------
     
       fahrkartenAusgeben(); //Ausgelagerte Methode in der der/die Fahrschein(e) ausgegeben werden/wird.
       
       
       System.out.println("\n\n");
       System.out.println("Vergessen sie nicht den Fahrschein vor der Fahrt zu entwerten.");
	   System.out.println("Gute Fahrt! :)");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
      
	   rueckgeldAusgeben(rückgabebetrag);
	   
	   main(args); //Das ist die Implementierung welche das Programm nach der Benutzung neu startet.
	   
    } 
    
    public static double fahrkartenbestellungErfassen() {//Ausgelagerte Methode zur Erfassung der Bestellung
    System.out.println("Fahrkartenbestellvorgang");
    System.out.println("========================\n");
    Scanner tastatur = new Scanner(System.in);
    int ticketAuswahl = 0;
    double endpreis = 0;
    double preis = 0;
    double anzahlTickets = 0;
    
    do{	//Beginn der Fußgesteuerten Schleife für den Bestellvorgang bis Benutzer den Vorgang mit "9" beendet.   	   
    fahrkartenauswahl(); //Einfügen der Methode in den Bestellvorgang.
    System.out.print("Ihre Wahl: ");	
    ticketAuswahl = tastatur.nextInt();
      
    if (ticketAuswahl == 1) 		//Wenn Benutzer "1" eingibt wird mit dem Wert 2.90 gerechnet.
    	preis = 2.90;
    	
    else if (ticketAuswahl == 2) 	//Wenn Benutzer "2" eingibt wird mit dem Wert 8.60 gerechnet.
    	preis = 8.60;
    
    else if (ticketAuswahl == 3) 	//Wenn Benutzer "2" eingibt wird mit dem Wert 23.50 gerechnet.
    	preis = 23.50;
    
    else if (ticketAuswahl == 9)
    
    while(ticketAuswahl != 9 && anzahlTickets == 0) { //Beginn der Kopfgesteuerten Schleife um die erneute Abfrage der Anzahl der Tickets nach beendetem Bestellvorgang.
    	
    
    
    System.out.println("Bitte geben Sie die Anzahl der Tickets ein: "); //Es sind keine Parameter vorhanden, jedoch übergibt sie die Anzahl der Tickets als Return.
    
    anzahlTickets = tastatur.nextDouble();
    while (anzahlTickets > 10 || anzahlTickets < 1) {
        System.out.println("Die zugelassene Ticketmenge wurde nicht eingehalten.");
        System.out.println("Bitte geben sie erneut die Anzahl ein. Die Anzahl darf 0 nicht unterschreiten!");
        //kontrollstruktur "if" wurde durch eine Schleife "while" ersetzt.
        //Benutzer wird nun aufgefordert eine richtige Eingabe zu tätigen, bis dies geschehen ist.
        anzahlTickets = tastatur.nextInt();
        }
    }
    endpreis = endpreis + (anzahlTickets * preis);
    } while (ticketAuswahl != 9); //Ende der Fußgesteuerten Schleife.
    return endpreis;
    }
      
    public static void fahrkartenauswahl(){		//Hier wird die Auswahl der Fahrkarte aufgerufen.
    	System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus: ");
    	System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");	//Hier stehen die Preise des jeweiligen Tickets.
    	System.out.println("Tageskarte Regeltarif AB [8,60] (2)");
    	System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
    	System.out.println("Wenn Sie bezahlen möchten, wählen Sie die (9)");	
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {  //Ausgelagerte Methode für die Bezahlung der Fahrkarten.
    Scanner tastatur = new Scanner(System.in);							 //Die Parameter sind ein zu zahlender Betrag als double.
    																	 //Gibt den Rückgabebetrag zurück.
    double eingezahlterGesamtbetrag = 0.0;
    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
    {
 	   System.out.printf("Noch zu zahlen: %.2f €\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag)); //Berechnung für den zu zahlenden Betrag.
 	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
 	   double eingeworfeneMünze = tastatur.nextDouble();
        eingezahlterGesamtbetrag += eingeworfeneMünze;
    }   
    double rueckgabeBetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    return rueckgabeBetrag;
   
    }
    
    public static void fahrkartenAusgeben() { //Ausgelagerte Methode ohne Paramemter.
    										  //Sie gibt nichts zurück.
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           
       warte(250); // Das Programm soll jeweils 250 ms Warten bis das nächste Symbol angezeigt wird.
       
     }
   }
  //Aufgabe 4++
    public static void warte(int ms) {
        try {
     	   Thread.sleep(ms);		//Befehl für die Wartezeit ohne Rückgabe
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) { 	//Methode um das Rückgeld auszugeben. 
      																//Die parameter sind Rückbetrag als double
    	 if(rückgabebetrag > 0.0)									//Sie gibt nichts zurück.
         {
      	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f €\n" , rückgabebetrag);
      	   System.out.println("wird in folgenden Münzen ausgezahlt:");

             while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
             {
            	 muenzeAusgeben(2, "Euro");
  	          rückgabebetrag -= 2.0;
             }
             while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
             {
            	 muenzeAusgeben(1, "Euro");
  	          rückgabebetrag -= 1.0;
             }
             while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
             {
            	 muenzeAusgeben(50, "Cent");
  	          rückgabebetrag -= 0.5;
             }
             while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
             {
            	 muenzeAusgeben(20, "Cent");
   	          rückgabebetrag -= 0.2;
             }
             while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
             {
            	 muenzeAusgeben(10, "Cent");
  	          rückgabebetrag -= 0.1;
             }
             while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
             {
            	 muenzeAusgeben(5, "Cent");
   	          rückgabebetrag -= 0.05;
             }
             while(rückgabebetrag >= 0.02)// 2 CENT-Münzen
             {
            	 muenzeAusgeben(2, "Cent");
   	          rückgabebetrag -= 0.02;
             }
             while(rückgabebetrag >= 0.01)// 1 CENT-Münzen
             {
            	 muenzeAusgeben(1, "Cent");
   	          rückgabebetrag -= 0.01;
             }
         }
    }
    //Aufgabe 4++ 2
    public static void muenzeAusgeben(int betrag, String einheit){  //Methode zur Ausgebe des Wechselgeldes in Münzen.
    	System.out.println(betrag + " " + einheit);					//Die Parameter sind Betrag als Int und die Einheit als String. 
    }
}