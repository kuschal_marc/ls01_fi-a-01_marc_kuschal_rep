import java.util.Scanner;

public class Summe {

	
		public static void main(String[] args) {
			
			Scanner sc = new Scanner(System.in);
			
			int summeBis = 0;
			int summe;
			int zaehler = 0;
			
			System.out.println("Geben Sie eine Zahl ein, bis welche addiert werden soll:");
			summeBis = sc.nextInt();
			
			//Kopfgesteuerte Schleife
			
			while(zaehler <= summeBis) {
				summe = summe + zaehler;
				zaehler++;
			}
			
			summe = 0;
			zaehler = 0;
			
			//Fu�gesteuerte Schleife
			
			do {
				
				summe = summe + zaehler;
				zaehler = zaehler + 1;
				
			}while(zaehler <= summe);
			
			System.out.println(summe);
			
			
		}
}
