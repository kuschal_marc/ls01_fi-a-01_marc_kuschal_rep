
public class Fakult�t {

	public static void main(String[] args) {
		System.out.println("   **   ");
		System.out.println("*      *");
		System.out.println("*      *");
		System.out.println("   **   ");
		System.out.println("");
		
		System.out.printf("0! %5s %19s %4s %n","=", "=", "1");
		System.out.printf("1! %7s %17s %4s %n","= 1", "=", "1");
		System.out.printf("2! %11s %13s %4s %n","= 1 * 2", "=", "2");
		System.out.printf("3! %15s %9s %4s %n","= 1 * 2 * 3", "=", "6");
		System.out.printf("4! %19s %5s %4s %n","= 1 * 2 * 3 * 4", "=", "24");
		System.out.printf("5! %23s %1s %4s %n %n","= 1 * 2 * 3 * 4 * 5", "=", "120");

		System.out.printf("%-12s | %10s %n", "Fahrenheit", "Celsius");
		System.out.printf("-------------------------- %n");
		System.out.printf("%+-12d | %+10.2f %n", -20, -28.89);
		System.out.printf("%+-12d | %+10.2f %n", -10, -23.3333);
		System.out.printf("%+-12d | %+10.2f %n", 0, -17.7778);
		System.out.printf("%+-12d | %+10.2f %n", 20, -6.6667);
		System.out.printf("%+-12d | %+10.2f %n", 30, -1.1111);

	}

}
